import React, { Component } from 'react';
import './App.css';
import Counter from './counter-testing/Counter';

class App extends Component {
  render() {
    return <Counter />;
  }
}

export default App;
